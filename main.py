import sys
import grabber
import reader


def main():
    arch = grabber.parse_args()
    contents = grabber.grab(arch)

    reader.print_top(contents, 10)


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        sys.exit(e)
