import argparse
import io
import os
import wget
import gzip


def parse_args() -> str:
    """
    Parses the commandline arguments for the architecture.

    :return: Name of architecture (formatted for use in grab())
    """

    ARCHS = ['all', 'amd64', 'arm64', 'armel', 'armhf', 'i386', 'mips64el', 'mipsel', 'ppc64el', 's390x']

    parser = argparse.ArgumentParser(
        description='Outputs the statistics of the top 10 packages that have the most files'
                    ' associated with them for a particular architecture.')
    parser.add_argument('arch', help=str(ARCHS))

    arch = parser.parse_args().arch.lower()
    if arch in ARCHS:
        return arch
    else:
        raise ValueError("The inputted architecture is invalid.")


def grab(arch: str) -> io.TextIOWrapper:
    """
    Grabs the contents file for the specified architecture.

    :return: TextIOWrapper of contents file
    """

    mirror = "http://ftp.uk.debian.org/debian/dists/stable/main/"
    url = mirror + "Contents-" + arch + ".gz"

    try:
        print("Downloading Contents...")
        # Download .gz
        compressed_contents = wget.download(url)
        # Extract .gz
        contents = gzip.open(compressed_contents, mode="rt")
        # Delete .gz
        os.remove(wget.filename_from_url(url))
        print("\nDone.")
    except Exception as e:
        raise e

    return contents
