# Canonical Interview Question: Debian Package Statistics

## Instructions

Debian uses *deb packages to deploy and upgrade software. The packages
are stored in repositories and each repository contains the so called "Contents
index". The format of that file is well described here
https://wiki.debian.org/RepositoryFormat#A.22Contents.22_indices

Your task is to develop a python command line tool that takes the
architecture (amd64, arm64, mips etc.) as an argument and downloads the
compressed Contents file associated with it from a Debian mirror. The
program should parse the file and output the statistics of the top 10
packages that have the most files associated with them.
An example output could be:

./package_statistics.py amd64

1. <package name 1>         <number of files>
2. <package name 2>         <number of files>
......
10. <package name 10>         <number of files>

You can use the following Debian mirror
http://ftp.uk.debian.org/debian/dists/stable/main/. Please do try to
follow Python's best practices in your solution. Hint: there are tools
that can help you verify your code is compliant. In-line comments are
appreciated.

It will be good if the code is accompanied by a 1-page report of the
work that you have done including the time you actually spent working on it.

Once started, please return your work in approximately 24 hours.

Note: the focus is not to write the perfect Python code, but to see how
you'll approach the problem and how you organize your work.

## My approach
My approach involves the following Python modules:
1. grabber.py - Parses the program's arguments, then if the architecture is
valid, grabs and extracts the Contents file for the specified architecture.
2. reader.py - Reads the Contents file and makes a Dictionary of the amount of
files associated with an architecture. Prints the formatted results.

Currently, this solution only follows the specifications and nothing more.
This program could be easily expanded to allow for the user to input an
argument stating how many results they would like to see, but I will
omit this for now. This solution also does not include unit tests, but
will throw and exception and exit upon any error, whether that be user-input
related or server-access related.

## Solution time
Started at 9:25 AM, 1/24/21

Finished at 1:13 PM, 1/24/21

Python is not my most used language, so I had to brush up on its included
methods.